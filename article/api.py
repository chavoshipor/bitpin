from multiprocessing import context
from django.http import Http404
from article.models import Article, Vote
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView
from article.serializers import ArticleDetailSerializer, ArticleSerializer, VoteSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from django.db.models import Case, When, Avg, Value, IntegerField


class ArticleAPIListView(APIView):
    def get(self, request):
        articles = self.get_queryset()
        serializer = ArticleSerializer(articles, many=True)
        return Response(dict(body=serializer.data))

    def get_queryset(self):
        queryset = Article.objects.all()
        user = self.request.user
        queryset = queryset.annotate(
            user_vote=Avg(
                Case(
                    When(votes__id=user.id, then='vote_article__value'),
                    default=Value(None),
                    output_field=IntegerField(),
                )
            )
        )
        return queryset


class ArticleAPIView(APIView):
    def get_object(self, article_id):
        try:
            return Article.objects.get(pk=article_id)
        except Article.DoesNotExist:
            raise Http404

    def get(self, request, article_id):
        article = self.get_object(article_id)
        serializer = ArticleDetailSerializer(
            article, context={'request': request})
        return Response(serializer.data)


class VoteAPIView(CreateAPIView):
    queryset = Vote.objects.all()
    serializer_class = VoteSerializer

    def perform_create(self, serializer):
        article_id = self.kwargs['article_id']
        user = self.request.user
        vote = Vote.objects.filter(article_id=article_id, user=user).first()
        if vote:
            serializer.instance = vote
        serializer.save(article_id=article_id, user=user)
