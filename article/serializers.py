from rest_framework import serializers
from django.db.models import Avg

from article.models import Article, Vote


class ArticleSerializer(serializers.ModelSerializer):
    vote_count = serializers.SerializerMethodField()
    average_vote = serializers.SerializerMethodField()
    user_vote = serializers.FloatField(read_only=True)

    class Meta:
        model = Article
        fields = ['id', 'title', 'content',
                  'vote_count', 'average_vote', 'user_vote']

    def get_vote_count(self, obj):
        return obj.votes.count()

    def get_average_vote(self, obj):
        votes = Vote.objects.filter(article=obj)
        if not votes:
            return 0
        return sum(vote.value for vote in votes) / votes.count()


class ArticleDetailSerializer(serializers.ModelSerializer):
    vote_count = serializers.SerializerMethodField()
    average_vote = serializers.SerializerMethodField()
    user_vote = serializers.SerializerMethodField()

    class Meta:
        model = Article
        fields = ['id', 'title', 'content',
                  'vote_count', 'average_vote', 'user_vote']

    def get_vote_count(self, obj):
        return obj.votes.count()

    def get_average_vote(self, obj):
        votes = Vote.objects.filter(article=obj)
        if not votes:
            return 0
        return sum(vote.value for vote in votes) / votes.count()

    def get_user_vote(self, obj):
        user = self.context.get('request').user
        try:
            return Vote.objects.filter(article=obj).filter(user=user).first().value
        except Exception:
            return 0


class VoteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vote
        fields = ['id', 'value']
