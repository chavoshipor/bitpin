from django.urls import include, path
from rest_framework import routers

from article.views import ArticleDetailView, ArticleListView, not_found_page


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path("404/", not_found_page, name="notfound_page"),
    path("articles", ArticleListView.as_view(), name="articles_view_list"),
    path("article/<int:article_id>", ArticleDetailView.as_view(),
         name="articles_view_detail")
]
