from django.shortcuts import render
from django.views.generic.list import ListView

from article.models import Article


class ArticleListView(ListView):
    model = Article
    template_name = 'article_list.html'


def not_found_page(request):
    return render(request, "404.html")


class ArticleDetailView(ListView):
    model = Article
    template_name = 'article_detail.html'
