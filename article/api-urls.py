from django.urls import include, path
from rest_framework import routers

from article.api import ArticleAPIListView, ArticleAPIView, VoteAPIView
from django.views.decorators.csrf import csrf_exempt


router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('articles/', ArticleAPIListView.as_view(), name='article_list'),
    path('articles/<int:article_id>/', ArticleAPIView.as_view(), name='article_detail'),
    path('articles/<int:article_id>/vote/', VoteAPIView.as_view(), name='vote_article'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]