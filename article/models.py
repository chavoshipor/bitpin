from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


class Article(models.Model):
    title = models.CharField(max_length=350, blank=False, null=False)
    thumbnail = models.ImageField(upload_to='thumbnails/')
    content = models.TextField()
    votes = models.ManyToManyField('auth.User', through='Vote', related_name="votes_article")


    def average_vote(self):
        return self.votes.aggregate(models.Avg('vote__value'))['vote__value__avg']


class Vote(models.Model):
    user = models.ForeignKey(
        User, related_name="vote_user", on_delete=models.CASCADE)
    article = models.ForeignKey(
        Article, related_name="vote_article", on_delete=models.CASCADE)
    value = models.IntegerField(
        validators=[MinValueValidator(0), MaxValueValidator(5)])
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'article',)
